(ns window.subs
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
  :current-temperature
  (fn [db _]
    (get-in db [:currently :temperature])))

(reg-sub
 :current-summary
 (fn [db _]
   (get-in db [:currently :summary])))

(reg-sub
 :hourly-summary
 (fn [db _]
   (get-in db [:hourly :summary])))

(reg-sub
 :hourly-data
 (fn [db [_ num]]
   (take num (get-in db [:hourly :data]))))

(reg-sub
 :city-name
 (fn [db _]
   (:city-name db)))
