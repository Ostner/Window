(ns window.styles)

(def styles {:today-view {:flex 1
                          :flex-direction "column"
                          :margin 40
                          :align-items "center"
                          :justify-content "space-between"}

             :container1 {:align-items "center"}

             :temperature-text {:font-size 120
                                :font-weight "100"
                                :margin-bottom 20
                                :text-align "center"
                                }

             :city-name {:font-weight "100"}

             :hour-text {:font-size 30 :padding 15}

             :small-temperature-text {:font-size 30 :padding 15}

             :precip-prob-text {:font-size 30 :padding 15}})
