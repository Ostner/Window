(ns window.db
  (:require [clojure.spec.alpha :as s]))

;; spec of app-db
(s/def ::city-name string?)
(s/def ::latitude double?)
(s/def ::longitude double?)

(s/def ::temperature double?)
(s/def ::summary string?)
(s/def ::currently
  (s/keys :req-un [::temperature
                   ::summary]))

(s/def ::time int?)
(s/def ::precipProbability double?)
(s/def ::cloudCover double?)
(s/def ::hour (s/keys :req-un [::time
                               ::temperature
                               ::precipProbability
                               ::cloudCover
                               ::summary]))
(s/def ::data (s/coll-of ::hour))
(s/def ::hourly
  (s/keys :req-un [::summary
                   ::data]))

(s/def ::app-db
  (s/keys :req-un [::city-name
                   ::latitude
                   ::longitude
                   ::currently
                   ::hourly]))

;; initial state of app-db
(def app-db {:city-name "unknown city"
             :latitude 0
             :longitude 0
             :currently {:summary ""
                         :temperature -99}
             :hourly {:summary ""
                      :data [{:time 0
                              :temperature -99
                              :precipProbability 0
                              :cloudCover 0
                              :summary ""}]}})
