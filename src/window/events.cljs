(ns window.events
  (:require
   [re-frame.core :refer [reg-event-db reg-event-fx after]]
   [clojure.spec.alpha :as s]
   [window.db :as db :refer [app-db]]
   [ajax.core :as ajax]
   [day8.re-frame.http-fx]
   [window.api-key :refer [api-key]]
   [window.test-data :refer [test-data]]))

;; -- Interceptors ------------------------------------------------------------
;;
;; See https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md
;;
(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db [event]]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check after " event " failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (after (partial check-and-throw ::db/app-db))
    []))

;; -- Handlers --------------------------------------------------------------

(reg-event-db
 :initialize-db
 validate-spec
 (fn [_ _]
   app-db))

(reg-event-db
 :request-success
 validate-spec
 (fn [db [_ {:keys [currently hourly]}]]
     (prn "successful http request")
   (let [{h-sum :summary h-data :data} hourly]
     (assoc db
            :currently (select-keys currently [:summary :temperature])
            :hourly {:summary h-sum
                     :data (mapv
                            #(select-keys % [:time
                                             :temperature
                                             :precipProbability
                                             :cloudCover
                                             :summary])
                            h-data)}))))

(reg-event-fx
 :request-failure
 validate-spec
 (fn [_ result]
   (prn "failed http request")
   (prn result)))

(reg-event-fx
 :load-weather
 validate-spec
 (fn [_ _]
   {:http-xhrio {:method :get
                 :uri (str "https://api.darksky.net/forecast/"
                           api-key "/"
                           (test-data :latitude) "," (test-data :longitude)
                           "?" "units=auto" "&lang=de")
                 :timeout 8000
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success [:request-success]
                 :on-failure [:request-failure]}}))
