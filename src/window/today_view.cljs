(ns window.today-view
  (:require [window.styles :refer [styles]]
            [re-frame.core :refer [dispatch subscribe]]
            [window.components :refer [text view touchable-highlight scroll-view]]
            [goog.string :as gs]
            [goog.string.format]
            [cljs-time.coerce :refer [from-long]]
            [cljs-time.core :refer [hour]]))

(defn- time->text [t]
  (-> t
      (* 1000)
      from-long
      hour
      (str ":00")))

(defn- temperature->text [t]
  (gs/format "%.0f°" t))

(defn- prob->text [p]
  (gs/format "%.0f%" (* 100 p)))

(defn today-view []
  (let [curr-temp (subscribe [:current-temperature])
        curr-sum (subscribe [:current-summary])
        h-sum (subscribe [:hourly-summary])
        h-data (subscribe [:hourly-data 12])]
    (fn []
      [scroll-view
       [view {:style (styles :today-view)}

        [view {:style (styles :container1)}
         [text {:style (styles :temperature-text)}
          (temperature->text @curr-temp)]
         [text @curr-sum]
         [text @h-sum]]

        [view
         (for [h @h-data]
           ^{:key (:time h)}
           [view {:style {:flex-direction :row}}
            [text {:style (styles :hour-text)}
             (time->text (:time h))]

            [view
             [text {:style {:text-align "center"}} (:summary h)]
             [view {:style {:flex-direction :row}}
              [text {:style (styles :small-temperature-text)}
               (temperature->text (:temperature h))]
              [text {:style (styles :precip-prob-text)}
               (prob->text (:precipProbability h))]
              [text {:style (styles :precip-prob-text)}
               (prob->text (:cloudCover h))]]]])]

        [touchable-highlight
         {:on-press #(dispatch [:load-weather])}
         [text "load weather"]]]])))
