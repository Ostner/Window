(ns window.ios.core
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch-sync]]
            [window.events]
            [window.subs]
            [window.components :refer [ReactNative]]
            [window.today-view :refer [today-view]]))

(def app-registry (.-AppRegistry ReactNative))

(defn app-root []
  [today-view])

(defn init []
  (dispatch-sync [:initialize-db])
  (.registerComponent app-registry "Window" #(r/reactify-component app-root)))
